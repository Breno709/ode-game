﻿using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class MateriaSaveManager
{
    public string YmirSavePath { get; set; }
    public string OydSavePath { get; set; }

    public MateriaSaveManager(string caminhoYmir, string caminhoOyd)
    { YmirSavePath = caminhoYmir; OydSavePath = caminhoOyd; }

    /// <summary>
    /// Salva o novo Ymir no banco de dados
    /// </summary>
    /// <param name="ymir">Ymir a ser salvo</param>
    public void AddYmir(Ymir ymir)
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Append, FileAccess.Write);
        BinaryFormatter fom = new BinaryFormatter();
        
        try {
            fom.Serialize(IOManager, ymir);
            IOManager.Close();
        }
        catch { IOManager.Close(); }
    }

    public void ConfirmYmirChanges(Ymir[] ymiris)
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Create, FileAccess.Write);
        BinaryFormatter fom = new BinaryFormatter();

        foreach(Ymir ymir in ymiris)
        { fom.Serialize(IOManager, ymir); }

        IOManager.Close();
    }

    /// <summary>
    /// Salva o novo Oyd no banco de dados
    /// </summary>
    /// <param name="oyd">Oyd a ser salvo</param>
    public void AddOyd(Oyd oyd)
    {
        FileStream IOManager = new FileStream(OydSavePath, FileMode.Append, FileAccess.Write);
        BinaryFormatter fom = new BinaryFormatter();

        try
        {
            fom.Serialize(IOManager, oyd);
            IOManager.Close();
        }
        catch { IOManager.Close(); }
    }

    /// <summary>
    /// Obtem todos os Ymir salvos
    /// </summary>
    /// <returns></returns>
    public Ymir[] GetYmir()
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        List<Ymir> ymir = new List<Ymir>();

        while (IOManager.Position < IOManager.Length)
        { ymir.Add(fom.Deserialize(IOManager) as Ymir); }

        IOManager.Close();
        return ymir.ToArray();
    }

    /// <summary>
    /// Obtem todos os Oyd Salvos
    /// </summary>
    /// <returns></returns>
    public Oyd[] GetOyd()
    {
        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        List<Oyd> oyd = new List<Oyd>();

        while (IOManager.Position < IOManager.Length)
        { oyd.Add(fom.Deserialize(IOManager) as Oyd); }

        IOManager.Close();
        return oyd.ToArray();
    }

    public Ymir FindYmirByName(string nome)
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Ymir tmpYmir, restYmir = new Ymir();

        while (IOManager.Position < IOManager.Length)
        {
            tmpYmir = (fom.Deserialize(IOManager) as Ymir);
            if (tmpYmir.Nome == nome) restYmir = tmpYmir;  break;
        }

        IOManager.Close();
        return restYmir;
    }

    /// <summary>
    /// Busca no banco de dados Ymir com mesmo nivel joulico
    /// </summary>
    /// <param name="nivelJoule">Nivel joule a ser consultado</param>
    /// <param name="Range">taxa de erro (em porcentagem) a ser considerada. EX.: 10 = +/- 10%</param>
    /// <returns>Ymir com mesmo nivel joulico</returns>
    public Ymir FindYmirByNivelJoule(float nivelJoule, int Range)
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Ymir tmpYmir, restYmir = new Ymir();

        while (IOManager.Position < IOManager.Length)
        {
            tmpYmir = (fom.Deserialize(IOManager) as Ymir);
            if ((tmpYmir.NivelJoule - ((tmpYmir.NivelJoule/100) * Range) >= nivelJoule) && (tmpYmir.NivelJoule + ((tmpYmir.NivelJoule / 100) * Range) <= nivelJoule))
                restYmir = tmpYmir; break;
        }

        IOManager.Close();
        return restYmir;
    }

    /// <summary>
    /// Busca no banco de dados todos Ymiris com mesmo nivel joulico comparado
    /// </summary>
    /// <param name="nivelJoule">Nivel joule a ser consultado</param>
    /// <param name="Range">taxa de erro (em porcentagem) a ser considerada. EX.: 10 = +/- 10%</param>
    /// <returns>Ymiris com mesmo nivel joulico</returns>
    public Ymir[] FindYmirisByNivelJoule(float nivelJoule, int Range)
    {
        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Ymir tmpYmir;
        List<Ymir> result = new List<Ymir>();

        while (IOManager.Position < IOManager.Length)
        {
            tmpYmir = (fom.Deserialize(IOManager) as Ymir);
            if ((tmpYmir.NivelJoule - ((tmpYmir.NivelJoule / 100) * Range) >= nivelJoule) && (tmpYmir.NivelJoule + ((tmpYmir.NivelJoule / 100) * Range) <= nivelJoule))
                result.Add(tmpYmir);
        }

        IOManager.Close();
        return result.ToArray();
    }

    /// <summary>
    /// Busca no banco de dados o primeiro Ymir com todos os parametros correspondentes
    /// </summary>
    /// <param name="customProperties">Propriedades a serem verificadas</param>
    /// <param name="values">Valores a serem comparados com as propriedades</param>
    public Ymir FindYmirByCustomProperty(string[] customProperties, params object[] values)
    {
        if (customProperties.Length != values.Length)
        { throw new System.Exception("Quantidade de propriedades a serem verificadas difere da quantidade de valores"); }

        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Ymir tmpYmir, restYmir = new Ymir();
        bool stopSearch = false;

        while (IOManager.Position < IOManager.Length)
        {
            tmpYmir = (fom.Deserialize(IOManager) as Ymir);
            int index = 0;
            foreach (string property in customProperties)
            {
                switch (property.ToUpper().Replace(" ",""))
                {
                    case "NOME":
                        if (tmpYmir.Nome != values[index] as string) goto NextYmir;
                        break;

                    case "NIVELJOULE":
                        if (tmpYmir.NivelJoule != values[index] as float?) goto NextYmir;
                        break;

                    case "CARGAMOLAR":
                        if (tmpYmir.CargaMolar != values[index] as float?) goto NextYmir;
                        break;

                    case "ALMIN":
                        if (tmpYmir.Almin != values[index] as Alma?) goto NextYmir;
                        break;

                    case "REACAOCADEIA":
                        if (tmpYmir.ReacaoCadeia != values[index] as Chain?) goto NextYmir;
                        break;

                    case "TEMPOFUNC1":
                        if (tmpYmir.TempoFunc1 != values[index] as int?) goto NextYmir;
                        break;

                    case "TEMPOREACAO":
                        if (tmpYmir.TempoReacao != values[index] as int?) goto NextYmir;
                        break;
                }

                if (index == customProperties.Length - 1)
                {  restYmir = tmpYmir; stopSearch = true; break; }

                index++;
            }

            if(stopSearch) { break; }

        NextYmir:;
        }
        IOManager.Close();
        return restYmir;
    }

    /// <summary>
    /// Busca no banco de dados todos os Ymiris com todos os parametros correspondentes
    /// </summary>
    /// <param name="customProperties">Propriedades a serem verificadas</param>
    /// <param name="values">Valores a serem comparados com as propriedades</param>
    public Ymir[] FindYmirisByCustomProperty(string[] customProperties, params object[] values)
    {
        if (customProperties.Length != values.Length)
        { throw new System.Exception("Quantidade de propriedades a serem verificadas difere da quantidade de valores"); }

        FileStream IOManager = new FileStream(YmirSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Ymir tmpYmir;
        List<Ymir> restYmiris = new List<Ymir>();

        while (IOManager.Position < IOManager.Length)
        {
            tmpYmir = (fom.Deserialize(IOManager) as Ymir);
            int index = 0;
            foreach (string property in customProperties)
            {
                switch (property.ToUpper().Replace(" ",""))
                {
                    case "NOME":
                        if (tmpYmir.Nome != values[index] as string) goto NextYmir;
                        break;

                    case "NIVELJOULE":
                        if (tmpYmir.NivelJoule != values[index] as float?) goto NextYmir;
                        break;

                    case "CARGAMOLAR":
                        if (tmpYmir.CargaMolar != values[index] as float?) goto NextYmir;
                        break;

                    case "ALMIN":
                        if (tmpYmir.Almin != values[index] as Alma?) goto NextYmir;
                        break;

                    case "REACAOCADEIA":
                        if (tmpYmir.ReacaoCadeia != values[index] as Chain?) goto NextYmir;
                        break;

                    case "TEMPOFUNC1":
                        if (tmpYmir.TempoFunc1 != values[index] as int?) goto NextYmir;
                        break;

                    case "TEMPOREACAO":
                        if (tmpYmir.TempoReacao != values[index] as int?) goto NextYmir;
                        break;
                }

                if (index == customProperties.Length - 1)
                { restYmiris.Add(tmpYmir); break; }

                index++;
            }

        NextYmir:;
        }
        IOManager.Close();
        return restYmiris.ToArray();
    }


    public Oyd FindOydByName(string nome)
    {
        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Oyd tmpOyd, restOyd = new Oyd();

        while (IOManager.Position < IOManager.Length)
        {
            tmpOyd = (fom.Deserialize(IOManager) as Oyd);
            if (tmpOyd.Nome == nome) restOyd = tmpOyd; break;
        }

        IOManager.Close();

        return restOyd;
    }

    /// <summary>
    /// Busca no banco de dados Oyd com mesmo nivel joulico
    /// </summary>
    /// <param name="nivelJoule">Nivel joule a ser consultado</param>
    /// <param name="Range">taxa de erro (em porcentagem) a ser considerada. EX.: 10 = +/- 10%</param>
    /// <returns>Oyd com mesmo nivel joulico</returns>
    public Oyd FindOydByNivelJoule(float nivelJoule, int Range)
    {
        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Oyd tmpOyd, restOyd = new Oyd();

        while (IOManager.Position < IOManager.Length)
        {
            tmpOyd = (fom.Deserialize(IOManager) as Oyd);
            if ((tmpOyd.NivelJoule - ((tmpOyd.NivelJoule / 100) * Range) >= nivelJoule) && (tmpOyd.NivelJoule + ((tmpOyd.NivelJoule / 100) * Range) <= nivelJoule))
                restOyd = tmpOyd; break;
        }

        IOManager.Close();
        return restOyd;
    }

    /// <summary>
    /// Busca no banco de dados todos Oyds com mesmo nivel joulico comparado
    /// </summary>
    /// <param name="nivelJoule">Nivel joule a ser consultado</param>
    /// <param name="Range">taxa de erro (em porcentagem) a ser considerada. EX.: 10 = +/- 10%</param>
    /// <returns>Ymiris com mesmo nivel joulico</returns>
    public Oyd[] FindOydsByNivelJoule(float nivelJoule, int Range)
    {
        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Oyd tmpOyd;
        List<Oyd> result = new List<Oyd>();

        while (IOManager.Position < IOManager.Length)
        {
            tmpOyd = (fom.Deserialize(IOManager) as Oyd);
            if ((tmpOyd.NivelJoule - ((tmpOyd.NivelJoule / 100) * Range) >= nivelJoule) && (tmpOyd.NivelJoule + ((tmpOyd.NivelJoule / 100) * Range) <= nivelJoule))
                result.Add(tmpOyd);
        }

        IOManager.Close();
        return result.ToArray();
    }

    /// <summary>
    /// Busca no banco de dados o primeiro Oyd com todos os parametros correspondentes
    /// </summary>
    /// <param name="customProperties">Propriedades a serem verificadas</param>
    /// <param name="values">Valores a serem comparados com as propriedades</param>
    public Oyd FindOydByCustomProperty(string[] customProperties, params object[] values)
    {
        if (customProperties.Length != values.Length)
        { throw new System.Exception("Quantidade de propriedades a serem verificadas difere da quantidade de valores"); }

        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Oyd tmpOyd, restOyd = new Oyd();
        bool stopSearch = false;

        while (IOManager.Position < IOManager.Length)
        {
            tmpOyd = (fom.Deserialize(IOManager) as Oyd);
            int index = 0;
            foreach (string property in customProperties)
            {
                switch (property.ToUpper().Replace(" ", ""))
                {
                    case "NOME":
                        if (tmpOyd.Nome != values[index] as string) goto NextOyd;
                        break;

                    case "NIVELJOULE":
                        if (tmpOyd.NivelJoule != values[index] as float?) goto NextOyd;
                        break;

                    case "CARGAMOLAR":
                        if (tmpOyd.CargaMolar != values[index] as float?) goto NextOyd;
                        break;

                    case "ALMIN":
                        if (tmpOyd.Almin != values[index] as Alma?) goto NextOyd;
                        break;

                    case "REACAOCADEIA":
                        if (tmpOyd.ReacaoCadeia != values[index] as Chain?) goto NextOyd;
                        break;

                    case "TEMPOFUNC1":
                        if (tmpOyd.TempoFunc1 != values[index] as int?) goto NextOyd;
                        break;

                    case "TEMPOREACAO":
                        if (tmpOyd.TempoReacao != values[index] as int?) goto NextOyd;
                        break;
                }

                if (index == customProperties.Length - 1)
                { restOyd = tmpOyd; stopSearch = true; break; }

                index++;
            }

            if (stopSearch) { break; }

        NextOyd:;
        }
        IOManager.Close();
        return restOyd;
    }

    /// <summary>
    /// Busca no banco de dados todos os Oyds com todos os parametros correspondentes
    /// </summary>
    /// <param name="customProperties">Propriedades a serem verificadas</param>
    /// <param name="values">Valores a serem comparados com as propriedades</param>
    public Oyd[] FindOydsByCustomProperty(string[] customProperties, params object[] values)
    {
        if (customProperties.Length != values.Length)
        { throw new System.Exception("Quantidade de propriedades a serem verificadas difere da quantidade de valores"); }

        FileStream IOManager = new FileStream(OydSavePath, FileMode.Open, FileAccess.Read);
        BinaryFormatter fom = new BinaryFormatter();
        Oyd tmpOyd;
        List<Oyd> restOyds = new List<Oyd>();

        while (IOManager.Position < IOManager.Length)
        {
            tmpOyd = (fom.Deserialize(IOManager) as Oyd);
            int index = 0;
            foreach (string property in customProperties)
            {
                switch (property.ToUpper().Replace(" ", ""))
                {
                    case "NOME":
                        if (tmpOyd.Nome != values[index] as string) goto NextOyd;
                        break;

                    case "NIVELJOULE":
                        if (tmpOyd.NivelJoule != values[index] as float?) goto NextOyd;
                        break;

                    case "CARGAMOLAR":
                        if (tmpOyd.CargaMolar != values[index] as float?) goto NextOyd;
                        break;

                    case "ALMIN":
                        if (tmpOyd.Almin != values[index] as Alma?) goto NextOyd;
                        break;

                    case "REACAOCADEIA":
                        if (tmpOyd.ReacaoCadeia != values[index] as Chain?) goto NextOyd;
                        break;

                    case "TEMPOFUNC1":
                        if (tmpOyd.TempoFunc1 != values[index] as int?) goto NextOyd;
                        break;

                    case "TEMPOREACAO":
                        if (tmpOyd.TempoReacao != values[index] as int?) goto NextOyd;
                        break;
                }

                if (index == customProperties.Length - 1)
                { restOyds.Add(tmpOyd); break; }

                index++;
            }

        NextOyd:;
        }
        IOManager.Close();
        return restOyds.ToArray();
    }
}