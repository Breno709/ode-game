﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;


public abstract class ODFStandardEncryptionEngine
{
    private static RSACryptoServiceProvider Get_SavedKey(string containerName)
    {
        CspParameters cp = new CspParameters();
        cp.KeyContainerName = containerName;

        return new RSACryptoServiceProvider(cp);
    }

    private static void EncryptAlgorithmValues(Aes storingData)
    {
        byte[] encryptedIV;
        byte[] encryptedKey;
        using (RSACryptoServiceProvider rsa = Get_SavedKey(PlayerPrefs.GetString("0x0cab")))
        {
            encryptedIV = rsa.Encrypt(storingData.IV, false);
            encryptedKey = rsa.Encrypt(storingData.Key, false);
        }

        PlayerPrefs.SetString("OEIV0", Convert.ToBase64String(encryptedIV));
        PlayerPrefs.SetString("OEK0", Convert.ToBase64String(encryptedKey));
    }
    private static Aes DecryptAlgorithmValues()
    {
        if (!PlayerPrefs.HasKey("OEK0"))
        {
            Aes myaes = Aes.Create();
            myaes.GenerateIV();
            myaes.GenerateKey();

            EncryptAlgorithmValues(myaes);
        }

        byte[] encryptedIV = Convert.FromBase64String(PlayerPrefs.GetString("OEIV0"));
        byte[] encryptedKey = Convert.FromBase64String(PlayerPrefs.GetString("OEK0"));

        using (RSACryptoServiceProvider rsa = Get_SavedKey(PlayerPrefs.GetString("0x0cab")))
        {
            Aes result = Aes.Create();
            result.Key = rsa.Decrypt(encryptedKey, false);
            result.IV = rsa.Decrypt(encryptedIV, false);

            return result;
        }
    }

    static public byte[] EncryptData(string dataToEncrypt)
    {
        Aes standAes = DecryptAlgorithmValues();
        ICryptoTransform encryptor = standAes.CreateEncryptor(standAes.Key, standAes.IV);

        return EncryptStringToBytes_Aes(dataToEncrypt, standAes.Key, standAes.IV);
    }

    static public byte[] EncryptData(byte[] dataToEncrypt)
    {
        Aes standAes = DecryptAlgorithmValues();
        ICryptoTransform encryptor = standAes.CreateEncryptor(standAes.Key, standAes.IV);

        return EncryptBytesToBytes_Aes(dataToEncrypt, standAes.Key, standAes.IV);
    }

    static public byte[] DecryptData(byte[] dataToDecrypt)
    {
        Aes standAes = DecryptAlgorithmValues();
        return DecrypBytesFromBytes_Aes(dataToDecrypt, standAes.Key, standAes.IV);
    }

    static public string DecryptDataIntoString(byte[] dataToDecrypt)
    {
        Aes standAes = DecryptAlgorithmValues();
        return DecrypStringFromBytes_Aes(dataToDecrypt, standAes.Key, standAes.IV);
    }

    #region PrivateOperation
    static private byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key,byte[] IV)
    {
        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
            throw new ArgumentNullException("plainText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");
        byte[] encrypted;
        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;

            // Create a decrytor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
        }


        // Return the encrypted bytes from the memory stream.
        return encrypted;

    }

    static private byte[] EncryptBytesToBytes_Aes(byte[] plainText, byte[] Key, byte[] IV)
    {
        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
            throw new ArgumentNullException("plainText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");
        byte[] encrypted;
        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;

            // Create a decrytor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
        }


        // Return the encrypted bytes from the memory stream.
        return encrypted;

    }

    static private byte[] DecrypBytesFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
    {
        // Check arguments.
        if (cipherText == null || cipherText.Length <= 0)
            throw new ArgumentNullException("cipherText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");

        // Declare the string used to hold
        // the decrypted text.
        byte[] plaintext;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;

            // Create a decrytor to perform the stream transform.
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting  stream
                        // and place them in a string.
                        plaintext = new byte[cipherText.Length];
                        srDecrypt.BaseStream.Read(plaintext, 0, plaintext.Length);
                    }
                }
            }

        }

        return plaintext;

    }

    static private string DecrypStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
    {
        // Check arguments.
        if (cipherText == null || cipherText.Length <= 0)
            throw new ArgumentNullException("cipherText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");

        // Declare the string used to hold
        // the decrypted text.
        string plaintext;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;

            // Create a decrytor to perform the stream transform.
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting  stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

        }

        return plaintext;
    }
    #endregion
}
