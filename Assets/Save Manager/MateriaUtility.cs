﻿using System;
using System.Collections.Generic;

public class MateriaUtility
{
    static public Ymir FindYmirByNivelJoule(Ymir[] ymirList, float nivelJoule, int Range)
    {
        Ymir restYmir = new Ymir();

        foreach (Ymir ymir in ymirList)
        {
            if ((ymir.NivelJoule - ((ymir.NivelJoule / 100) * Range) >= nivelJoule) && (ymir.NivelJoule + ((ymir.NivelJoule / 100) * Range) <= nivelJoule))
                restYmir = ymir; break;
        }

        return restYmir;
    }
    static public Ymir[] FindYmirisByNivelJoule(Ymir[] ymirList, float nivelJoule, int Range)
    {
        List<Ymir> result = new List<Ymir>();

        foreach(Ymir ymir in ymirList)
        {
            if ((ymir.NivelJoule - ((ymir.NivelJoule / 100) * Range) >= nivelJoule) && (ymir.NivelJoule + ((ymir.NivelJoule / 100) * Range) <= nivelJoule))
                result.Add(ymir);
        }

        return result.ToArray();
    }

    static public Oyd FindOydByNivelJoule(Oyd[] oydList, float nivelJoule, int Range)
    {
        Oyd restOyd = new Oyd();

        foreach (Oyd oyd in oydList)
        {
            if ((oyd.NivelJoule - ((oyd.NivelJoule / 100) * Range) >= nivelJoule) && (oyd.NivelJoule + ((oyd.NivelJoule / 100) * Range) <= nivelJoule))
                restOyd = oyd; break;
        }

        return restOyd;
    }
    static public Oyd[] FindOydsByNivelJoule(Oyd[] oydList, float nivelJoule, int Range)
    {
        List<Oyd> result = new List<Oyd>();

        foreach (Oyd oyd in oydList)
        {
            if ((oyd.NivelJoule - ((oyd.NivelJoule / 100) * Range) >= nivelJoule) && (oyd.NivelJoule + ((oyd.NivelJoule / 100) * Range) <= nivelJoule))
                result.Add(oyd);
        }

        return result.ToArray();
    }
}
