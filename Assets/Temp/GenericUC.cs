﻿using System;
using UnityEngine;
using System.Collections;

class GenericUC : MonoBehaviour
{
    public Materia GenericMateria { get; set; }
    public GenericUC GenericUnitCombat { get; set; }
    public int PlayerNumber;
    public int GenericValueToModify;

    void Start()
    {
        GenericMateria = new Fogo();
        
    }

    //public void Modify()
    //{
    //    GenericMateria.ReacaoSucceedEventHandler += OnReacted;
    //}

    //void OnReacted(object sender, ReacaoEventArgs e)
    //{
    //    GenericUC[] anothers = FindObjectsOfType<GenericUC>();
    //    anothers[1].GenericValueToModify = 10;
    //    Debug.Log("Funcao executada com Sucesso!");
    //}
}
