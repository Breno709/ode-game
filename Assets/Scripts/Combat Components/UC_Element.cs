﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Combat/Element")]
public class UC_Element : MonoBehaviour {

    /// <summary>
    /// Materia que denomina a UC
    /// </summary>
    public Materia Core { get; set; }
    public string Nome { get { return Core.Nome; } }
    public Teams Equipe { get; set; }
    public int BattleID { get { return 0; } }
    public float MateriaMol { get; set; }
}
