﻿using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Teams Equipe { get; set; }
    public GameObject UnidadeCombate;

    void Start()
    {
        
    }

    IEnumerator InstanciarUC()
    {
        while(!Input.GetMouseButtonDown(0))
        {
            Instantiate(UnidadeCombate, Input.mousePosition, Quaternion.Euler(0, 0, 0));
        }

        yield return null;
    }
}