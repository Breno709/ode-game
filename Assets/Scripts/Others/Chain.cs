﻿using System;
using UnityEngine;

/// <summary>
/// Enumeração possíveis reações em cadeia que podem acontecer
/// </summary>
public enum Chain
{
    None = 0,
    Labaredas=1,
    Erosão = 2
}