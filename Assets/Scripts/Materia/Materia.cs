﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public interface Materia {

    /// <summary>
    /// Nome da materia
    /// </summary>
    string Nome { get;}
    /// <summary>
    /// Alma da Materia. Responsável por indicar qual é a base da mesma.
    /// </summary>
	Alma Almin { get; }
    /// <summary>
    /// Nível de energia que se encontra a Materia
    /// </summary>
    float NivelJoule { get; set; }
    /// <summary>
    /// Carga molar que se encontra a Materia
    /// </summary>
    float CargaMolar { get; set; }
    /// <summary>
    /// Tempo (em turnos) que leva para ocorrer a reação 
    /// </summary>
    int TempoReacao { get; }
    /// <summary>
    /// Tempo (em turnos) que leva para ocorrer a primeira Função 
    /// </summary>
    int TempoFunc1 { get; }
    /// <summary>
    /// Tempo (em turnos) que leva para ocorrer a segunda Função 
    /// </summary>
    int TempoFunc2 { get; }
    /// <summary>
    /// Tempo (em turnos) que leva para ocorrer a terceira Função 
    /// </summary>
    int TempoFunc3 { get; }
    /// <summary>
    /// Possíveis reações
    /// </summary>
    Chain ReacaoCadeia { get; }

    /// <summary>
    /// Quando implementada, reage dois elementos para se criar uma nova Materia
    /// </summary>
    /// /// <param name="reagentes">Elementos usados para catalizar a reação</param>
    /// <param name="entradas">Elementos que se deseja reagir</param>
    /// <returns>Reação obtida</returns>
    void Reagir(Teams equipe, Materia[] reagentes ,params Materia[] entradas);
    /// <summary>
    /// Quando implementada, cria funções específicas de primeiro grau (mais fáceis de acontecer)
    /// </summary>
    void Funcao1(Teams equipe, params Materia[] parametros);
    /// <summary>
    /// Quando implementada, cria funções específicas de segundo grau (Acontecem com frequência moderada)
    /// </summary>
    void Funcao2(Teams equipe, params Materia[] parametros);
    /// <summary>
    /// Quando implementada, cria funções específicas de terceiro grau (Difíceis de acontecer)
    /// </summary>
    void Funcao3(Teams equipe, params Materia[] parametros);

    event EventHandler<ReacaoSucceedEventArgs> ReacaoSucceed;
}