﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class Oyd : Materia {

    public string Nome { get { return nome; } }
    public Alma Almin { get { return almin; } }
    public float NivelJoule { get; set; }
    public float CargaMolar { get; set; }
    public int TempoReacao { get { return tempoReacao; } }
    public int TempoFunc1 { get { return tempoFunc1; } }
    public int TempoFunc2 { get { return tempoFunc2; } }
    public int TempoFunc3 { get { return tempoFunc3; } }
    public Chain ReacaoCadeia { get { return reacaoCadeia; } }

    public virtual void Reagir(Teams equipe, Materia[] reagentes, params Materia[] entradas) { }
    public virtual void Funcao1(Teams equipe, params Materia[] parametros) { }
    public virtual void Funcao2(Teams equipe, params Materia[] parametros) { }
    public virtual void Funcao3(Teams equipe, params Materia[] parametros) { }

    private string nome;
    private Alma almin;
    private int tempoReacao;
    private int tempoFunc1;
    private int tempoFunc2;
    private int tempoFunc3;
    private Chain reacaoCadeia;

    public Oyd()
    {
        nome = "Materia Desconhecida";
        almin = Alma.Nata;
        NivelJoule = 0;
        CargaMolar = 0;
        tempoReacao = 0;
        tempoFunc1 = 0;
        tempoFunc2 = 0;
        tempoFunc3 = 0;
        reacaoCadeia = Chain.None;
    }
    public Oyd(string Pnome, Alma Palmin, float PnivelJoule, float PcargaMolar, int PtempoReacao, int PtempoFunc1, int PtempoFunc2, int PtempoFunc3, Chain PreacaoCadeia)
    {
        nome = Pnome;
        almin = Palmin;
        CargaMolar = PcargaMolar;
        tempoReacao = PtempoReacao;
        tempoFunc1 = PtempoFunc1;
        tempoFunc2 = PtempoFunc2;
        tempoFunc3 = PtempoFunc3;
        reacaoCadeia = PreacaoCadeia;
    }
    public Oyd(string Pnome, int Palmin, float PnivelJoule, float PcargaMolar, int PtempoReacao, int PtempoFunc1, int PtempoFunc2, int PtempoFunc3, Chain PreacaoCadeia)
    {
        nome = Pnome;
        almin = (Alma)Palmin;
        CargaMolar = PcargaMolar;
        tempoReacao = PtempoReacao;
        tempoFunc1 = PtempoFunc1;
        tempoFunc2 = PtempoFunc2;
        tempoFunc3 = PtempoFunc3;
        reacaoCadeia = PreacaoCadeia;
    }
    public Oyd(string Pnome, int Palmin, float PnivelJoule, float PcargaMolar, int PtempoReacao, int[] PtemposFunc, Chain PreacaoCadeia)
    {
        nome = Pnome;
        almin = (Alma)Palmin;
        CargaMolar = PcargaMolar;
        tempoReacao = PtempoReacao;
        tempoFunc1 = PtemposFunc[0];
        tempoFunc2 = PtemposFunc[1];
        tempoFunc3 = PtemposFunc[2];
        reacaoCadeia = PreacaoCadeia;
    }

    public event EventHandler<ReacaoSucceedEventArgs> ReacaoSucceed;

    protected virtual void OnReacaoSucceed(ReacaoSucceedEventArgs e)
    {
        EventHandler<ReacaoSucceedEventArgs> handler = ReacaoSucceed;
        if (handler != null)
        { handler(this, e); }
    }
}
