﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
[CreateAssetMenu(fileName ="Ymir", menuName ="Materia/Ymir", order = 1)]
public class Ymir : ScriptableObject, Materia {

	public string Nome { get { return nome; } }
    public Alma Almin { get { return almin; } }
    public float NivelJoule { get; set; }
    public float CargaMolar { get; set; }
    public int TempoReacao { get { return tempoReacao; } }
    public int TempoFunc1 { get { return tempoFunc1; } }
    public int TempoFunc2 { get { return 0; } }
    public int TempoFunc3 { get { return 0; } }
    public Chain ReacaoCadeia { get { return reacaoCadeia; } }

    public virtual void Reagir(Teams equipe, Materia[] reagentes, params Materia[] entradas) { }
    public virtual void Funcao1(Teams equipe, params Materia[] parametros) { }
    public void Funcao2(Teams equipe, params Materia[] parametros) { }
    public void Funcao3(Teams equipe, params Materia[] parametros) { }

    [SerializeField] private string nome;
    [SerializeField] private Alma almin;
    [SerializeField] private int tempoReacao;
    [SerializeField] private int tempoFunc1;
    [SerializeField] private Chain reacaoCadeia;
    [SerializeField] protected int reacaoCounter = 0, f1Counter = 0;

    public Ymir ()
    {
        nome = "Materia Desconhecida";
        almin = Alma.Nata;
        NivelJoule = 0;
        CargaMolar = 0;
        tempoReacao = 0;
        tempoFunc1 = 0;
        reacaoCadeia = Chain.None;
    }
    public Ymir (string Pnome, Alma Palmin, float PnivelJoule, float PcargaMolar, int PtempoReacao, int PtempoFuncao, Chain PreacaoCadeia)
    {
        nome = Pnome;
        almin = Palmin;
        NivelJoule = PnivelJoule;
        CargaMolar = PcargaMolar;
        tempoReacao = PtempoReacao;
        tempoFunc1 = PtempoFuncao;
        reacaoCadeia = PreacaoCadeia;
    }
    public Ymir (string Pnome, int Palmin, float PnivelJoule, float PcargaMolar, int PtempoReacao, int PtempoFuncao, Chain PreacaoCadeia)
    {
        nome = Pnome;
        almin = (Alma)Palmin;
        NivelJoule = PnivelJoule;
        CargaMolar = PcargaMolar;
        tempoReacao = PtempoReacao;
        tempoFunc1 = PtempoFuncao;
        reacaoCadeia = PreacaoCadeia;
    }

    public event EventHandler<ReacaoSucceedEventArgs> ReacaoSucceed;

    protected virtual void OnReacaoSucceed(ReacaoSucceedEventArgs e)
    {
        EventHandler<ReacaoSucceedEventArgs> handler = ReacaoSucceed;
        if(handler != null)
        { handler(this, e); }
    }
}