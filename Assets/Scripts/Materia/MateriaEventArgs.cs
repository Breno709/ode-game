﻿using System;
using UnityEngine;

public class ReacaoSucceedEventArgs : EventArgs
{
    public Materia Resultado { get; set; }
}