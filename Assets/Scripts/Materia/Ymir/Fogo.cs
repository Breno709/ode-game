﻿using System;
using UnityEngine;
using System.Collections;

[AddComponentMenu("Combat/Ymir/Fogo")]
class Fogo : Ymir
{
    public override void Reagir(Teams equipe, Materia[] reagentes, params Materia[] entradas)
    {
        switch(equipe)
        {
            case Teams.T1:
                GameController.Player1PhaseStarted += OnReacao;
                break;

            case Teams.T2:
                GameController.Player2PhaseStarted += OnReacao;
                break;
        }

        R_Reagentes = reagentes;
        R_Entradas = entradas;
    }

    private Materia[] R_Reagentes, R_Entradas;

    void OnReacao(object sender, Player1PhaseStartedEventArgs e)
    {
        reacaoCounter++;
        if (reacaoCounter >= TempoReacao)
        {
            Ymir result = MotorConversao.TransformacaoYmir.ConverterMateria(R_Reagentes[0], R_Entradas[0]);
            ReacaoSucceedEventArgs args = new ReacaoSucceedEventArgs();
            args.Resultado = result;

            OnReacaoSucceed(args);
            reacaoCounter = 0;
            GameController.Player1PhaseStarted -= OnReacao;
            R_Reagentes = new Materia[0];
            R_Entradas = new Materia[0];
        }
    }
    void OnReacao(object sender, Player2PhaseStartedEventArgs e)
    {
        reacaoCounter++;
        if (reacaoCounter >= TempoReacao)
        {
            Ymir result = MotorConversao.TransformacaoYmir.ConverterMateria(R_Reagentes[0], R_Entradas[0]);
            ReacaoSucceedEventArgs args = new ReacaoSucceedEventArgs();
            args.Resultado = result;

            OnReacaoSucceed(args);
            reacaoCounter = 0;
            GameController.Player2PhaseStarted -= OnReacao;
            R_Reagentes = new Materia[0];
            R_Entradas = new Materia[0];
        }
    }
}