﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.IO;

//Classe responsável por executar as conversões de matérias
public class MotorConversao {
    #region Deprecated
    List<BetaBase> registroBetas = new List<BetaBase>();

    public abstract class EBase
    {
        static public MateriaPreview ConverterMateria(MateriaPreview entrada, MateriaPreview almaEliminacao)
        {
            MateriaPreview resultado = new ElementoBase();
            resultado.Almin = AplicarEliminacao(entrada, almaEliminacao);
            resultado.NivelJoule = (almaEliminacao.Almin == Alma.Nata) ? entrada.NivelJoule - almaEliminacao.NivelJoule : entrada.NivelJoule + almaEliminacao.NivelJoule;

            return GerirMateria(resultado);
        }

        static private Alma AplicarEliminacao(MateriaPreview entrada, MateriaPreview eliminar)
        {
            int resultado = Mathf.Abs(entrada.Almin - eliminar.Almin);
            Alma resc;
            if (resultado != 1 && resultado != 2 && resultado != 3 && resultado != 10)
            { resultado = 0; }

            try { resc = (Alma)resultado; }
            catch { resc = Alma.Nata; }

            return resc;
        }

        /// <summary>
        /// Classifica a materia e coordena sua carga molar
        /// </summary>
        /// <param name="materiaDesconhecida"></param>
        /// <returns></returns>
        static private MateriaPreview GerirMateria(MateriaPreview materiaDesconhecida)
        {
            ElementoBase resultado = new ElementoBase();
            resultado.CargaMolar = 0;
            for (int i = 0; i < InfoBase.TodosJoules.Length; i++)
            {
                if (materiaDesconhecida.NivelJoule >= InfoBase.Min(InfoBase.TodosJoules[i]) && materiaDesconhecida.NivelJoule <= InfoBase.Max(InfoBase.TodosJoules[i]))
                {
                    if (materiaDesconhecida.Almin == InfoBase.ToAlmin(InfoBase.TodasAlmas[i]))
                    {
                        resultado = new ElementoBase(InfoBase.TodosNomes[i]);
                        resultado.CargaMolar = ((InfoBase.TodasCargamolar[i] / InfoBase.TodosJoules[i]) * materiaDesconhecida.NivelJoule);
                        break;
                    }
                }
            }

            resultado.NivelJoule = materiaDesconhecida.NivelJoule;
            resultado.Almin = materiaDesconhecida.Almin;
            return resultado;
        }
    }
   // static public Betas ConverterBeta(Materia entrada1, Materia entrada2, Materia energiaAcrescentar)
   // {
   // almin: entrada com maior nível joulico
   //Devido ao efeito de manter a Alma ao invés de aplicar eliminação, a quantidade de joule é subtraída da entrada1 pela entrada 2
   // }

    public abstract class EBeta
    {
        static public BetaBase ConverterBeta(MateriaPreview entrada1, MateriaPreview entrada2)
        {
            MateriaPreview x = (entrada1.NivelJoule > entrada2.NivelJoule) ? entrada1 : entrada2;
            MateriaPreview y = (entrada2.NivelJoule > entrada1.NivelJoule) ? entrada1 : entrada2;

            x.NivelJoule -= y.NivelJoule; //TODO: Terminar de construir este método

            return null;
        }

        //static private BetaBase GerirMateria(Materia entrada)
        //{
            
        //}

        //static private void LoadBetas()
        //{
        //    System.IO.TextReader textReader = (TextReader)new StreamReader(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\BetaRef.xml");
        //    XmlDocument betaDoc = new XmlDocument();
        //    XmlNodeList nos;
        //    betaDoc.Load(textReader);
        //    nos = betaDoc.SelectNodes("Beta");

        //    BetaBase resul = new BetaBase();
        //    foreach(XmlNode beta in nos)
        //    {
        //        string nome = beta["Nome"].Value;
        //        string afinidadeDestrutiva = beta["AfinidadeDestrutiva"].Value;
        //        string afinidadeDefensiva = beta["AfinidadeDefensiva"].Value;
        //        float cargaMolar = float.Parse(beta["CargaMolar"].Value);
        //        string alma = beta["Almin"].Value;
        //        float nivelJoule = float.Parse(beta["NivelJoule"].Value);
        //        beta.chi
        //        foreach(XmlNode criacao in beta.SelectNodes("Criacao"))
        //        //string[] criacao = {beta["Criacao"]}
        //    }
        //}
    }
    #endregion Deprecated

    public abstract class TransformacaoYmir
    {
        static public Ymir ConverterMateria(Materia entrada, Materia produto)
        {
            Alma almaRes = AplicarEliminacao(entrada.Almin, produto.Almin);
            float nJoule = (almaRes == Alma.Nata) ? entrada.NivelJoule - produto.NivelJoule : entrada.NivelJoule + produto.NivelJoule;

            Ymir resultado = new Ymir("", almaRes, nJoule, 0, 0, 0, 0);
            Ymir tmpYmir = Classificar(resultado);

            return tmpYmir;
        }

        static private Alma AplicarEliminacao(Alma entrada, Alma eliminar)
        {
            int resultado = Mathf.Abs(entrada - eliminar);
            Alma resc;
            if (resultado != 1 && resultado != 2 && resultado != 3 && resultado != 10)
            { resultado = 0; }

            try { resc = (Alma)resultado; }
            catch { resc = Alma.Nata; }

            return resc;
        }

        static private Ymir Classificar(Materia valor)
        {
            MateriaSaveManager msm = new MateriaSaveManager(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\YMIRDATA.ODF", null);
            Ymir[] candidates = msm.FindYmirisByCustomProperty(new string[1] {"almin"}, valor.Almin);
            Ymir resultado = MateriaUtility.FindYmirByNivelJoule(candidates, valor.NivelJoule, 10);

            Ymir resc = GetClassFunctions(resultado);
            return resc;
        }

        static private Ymir GetClassFunctions(Ymir ymirWithoutClassFunctions)
        {
            Type tipo = Type.GetType(ymirWithoutClassFunctions.Nome);
            object obj = tipo.InvokeMember(null, BindingFlags.DeclaredOnly |
            BindingFlags.Public | BindingFlags.NonPublic |
            BindingFlags.Instance | BindingFlags.CreateInstance, null, null, null);
            Ymir result = obj as Ymir;

            return result;
        }
    }
}
