﻿using System;
using UnityEngine;


public class GameController : MonoBehaviour
{
    static protected int Turn { get { return turn; }
        set {
            turn++;
            TurnChangedEventArgs args = new TurnChangedEventArgs();
            args.Turn = turn;

            OnTurnChanged(args);
        }
    }
    static protected int Phase
    {
        get { return phase; }
        set
        {
            PlayerPhaseChangedEventArgs argsPhase = new PlayerPhaseChangedEventArgs();
            switch(value)
            {
                case 1:
                    Player1PhaseStartedEventArgs argsp1 = new Player1PhaseStartedEventArgs();

                    phase = value;
                    argsPhase.ActualPlayer = phase;
                    OnPlayerPhaseChanged(argsPhase);
                    OnPlayer1PhaseStarted(argsp1);
                    break;

                case 2:
                    Player2PhaseStartedEventArgs argsp2 = new Player2PhaseStartedEventArgs();

                    phase = value;
                    argsPhase.ActualPlayer = phase;
                    OnPlayerPhaseChanged(argsPhase);
                    OnPlayer2PhaseStarted(argsp2);
                    break;

                default:
                    throw new Exception("Valores de jogadores excedem o limite!");
            }
        }
    }
    static private int turn = 0;
    static private int phase = 0;

    static public int GetPhase()
    { return Phase; }
    static public int GetTurnCount()
    { return Turn; }
    
    static public void AdvancePhase()
    {
        phase++;
        if (phase > 2)
        {
            Phase = 1;
            Turn++;
        }
        else Phase = phase;
    }

    void PrepareTeams()
    {

    }

    protected static void OnPlayerPhaseChanged(PlayerPhaseChangedEventArgs e)
    {
        EventHandler<PlayerPhaseChangedEventArgs> handler = PlayerPhaseChanged;
        if(handler != null)
        { handler(null, e); }
    }
    protected static void OnTurnChanged(TurnChangedEventArgs e)
    {
        EventHandler<TurnChangedEventArgs> handler = TurnChanged;
        if (handler != null)
        { handler(null, e); }
    }
    protected static void OnPlayer1PhaseStarted(Player1PhaseStartedEventArgs e)
    {
        EventHandler<Player1PhaseStartedEventArgs> handler = Player1PhaseStarted;
        if(handler != null)
        { handler(null, e); }
    }
    protected static void OnPlayer1PhaseEnded(Player1PhaseEndedEventArgs e)
    {
        EventHandler<Player1PhaseEndedEventArgs> handler = Player1PhaseEnded;
        if (handler != null)
        { handler(null, e); }
    }
    protected static void OnPlayer2PhaseStarted(Player2PhaseStartedEventArgs e)
    {
        EventHandler<Player2PhaseStartedEventArgs> handler = Player2PhaseStarted;
        if (handler != null)
        { handler(null, e); }
    }
    protected static void OnPlayer2PhaseEnded(Player2PhaseEndedEventArgs e)
    {
        EventHandler<Player2PhaseEndedEventArgs> handler = Player2PhaseEnded;
        if (handler != null)
        { handler(null, e); }
    }

    static public event EventHandler<PlayerPhaseChangedEventArgs> PlayerPhaseChanged;
    static public event EventHandler<TurnChangedEventArgs> TurnChanged;
    static public event EventHandler<Player1PhaseStartedEventArgs> Player1PhaseStarted;
    static public event EventHandler<Player1PhaseEndedEventArgs> Player1PhaseEnded;
    static public event EventHandler<Player2PhaseStartedEventArgs> Player2PhaseStarted;
    static public event EventHandler<Player2PhaseEndedEventArgs> Player2PhaseEnded;
}