﻿using System;

public class PlayerPhaseChangedEventArgs : EventArgs
{
    public int ActualPlayer { get { return actualPlayer; }
        set { if (value > 2 || value < 1) { throw new Exception("Número excede o limite!"); } actualPlayer = value;  } }

    private int actualPlayer;
}
public class TurnChangedEventArgs : EventArgs
{
    public int Turn { get; set; }
}
public class Player1PhaseStartedEventArgs : EventArgs { }
public class Player2PhaseStartedEventArgs : EventArgs { }
public class Player1PhaseEndedEventArgs : EventArgs { }
public class Player2PhaseEndedEventArgs : EventArgs { }