﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class YmirFab : EditorWindow
{
    string nome = "Nome";
    float nivelJoule = 0, cargaMolar = 0;
    int tempoReac = 0, tempoFunc1 = 0;
    Chain reacaoCadeia = Chain.None;
    Alma almin = Alma.Nata;
    Ymir[] savedData;
    string[] ymirNames;

    GUIStyle systemPresentation = new GUIStyle();

    [MenuItem("Factory/Ymir Factory")]
    static void Init()
    {
        YmirFab window = (YmirFab)EditorWindow.GetWindow(typeof(YmirFab));
        window.Show();
    }

    int systemOperation = 0;
    void OnGUI()
    {
        loadStation();
        EditorGUILayout.LabelField("Sistema Gerador de Ymir", systemPresentation);
        switch(systemOperation)
        {
            case 0:
                if(GUILayout.Button("Adicionar Ymir")) { systemOperation = 1; }
                if (GUILayout.Button("Editar Ymir")) { systemOperation = 2; }

                EditorGUILayout.Separator();
                if (GUILayout.Button("Sair", GUILayout.Width(50))) { Close(); }
                break;

            case 1:
                AddYmirLayout();
                EditorGUILayout.Separator();
                if (GUILayout.Button("Voltar", GUILayout.Width(50))) { systemOperation = 0; loadedData = false; }
                break;

            case 2:
                EditYmirLayout();
                EditorGUILayout.Separator();
                if (GUILayout.Button("Voltar", GUILayout.Width(50))) { systemOperation = 0; loadedData = false; }
                break;
        }
    }

    private void AddYmirLayout()
    {
        nome = ConstructTextField("Nome", nome);
        almin = (Alma)ConstructEnumField("Almin", almin);
        nivelJoule = ConstructFloatField("Nivel Joule", nivelJoule, 500);
        cargaMolar = ConstructFloatField("Carga Molar", cargaMolar, 50);
        tempoReac = ConstructIntField("Tempo Reacao", tempoReac, 10);
        tempoFunc1 = ConstructIntField("Tempo Func1", tempoFunc1, 10);
        reacaoCadeia = (Chain)ConstructEnumField("Reação Cadeia", reacaoCadeia);

        if(GUILayout.Button("Salvar Ymir"))
        {
            MateriaSaveManager msm = new MateriaSaveManager(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\YMIRDATA.ODF", null);
            msm.AddYmir(new Ymir(nome, almin, nivelJoule, cargaMolar, tempoReac, tempoFunc1, reacaoCadeia));

            Debug.Log("Dados salvos com sucesso!");
        }
    }

    bool loadedData = false;
    int index = 0;
    private void EditYmirLayout()
    {
        if (!loadedData) LoadYmirData(true); loadedData = true;

        index = ConstructSelectionField(index, ymirNames, "Selecionar Ymir: ");
        if (GUILayout.Button("Editar"))
        {
            nome = savedData[index].Nome;
            almin = savedData[index].Almin;
            nivelJoule = savedData[index].NivelJoule;
            cargaMolar = savedData[index].CargaMolar;
            tempoReac = savedData[index].TempoReacao;
            tempoFunc1 = savedData[index].TempoFunc1;
            reacaoCadeia = savedData[index].ReacaoCadeia;
        }

        nome = ConstructTextField("Nome", nome);
        almin = (Alma)ConstructEnumField("Almin", almin);
        nivelJoule = ConstructFloatField("Nivel Joule", nivelJoule, 500);
        cargaMolar = ConstructFloatField("Carga Molar", cargaMolar, 50);
        tempoReac = ConstructIntField("Tempo Reacao", tempoReac, 10);
        tempoFunc1 = ConstructIntField("Tempo Func1", tempoFunc1, 10);
        reacaoCadeia = (Chain)ConstructEnumField("Reação Cadeia", reacaoCadeia);

        if (GUILayout.Button("Salvar"))
        {
            savedData[index] = new Ymir(nome, almin, nivelJoule, cargaMolar, tempoReac, tempoFunc1, reacaoCadeia);
            MateriaSaveManager msm = new MateriaSaveManager(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\YMIRDATA.ODF", null);

            msm.ConfirmYmirChanges(savedData);
            Debug.Log("Dados salvos!");
            loadedData = false;
        }

        if (GUILayout.Button("Excluir"))
        {
            List<Ymir> tmp = new List<Ymir>();
            foreach(Ymir ymir in savedData) { tmp.Add(ymir); }
            tmp.RemoveAt(index);
            savedData = tmp.ToArray();

            MateriaSaveManager msm = new MateriaSaveManager(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\YMIRDATA.ODF", null);

            msm.ConfirmYmirChanges(savedData);
            Debug.Log("Ymir excluido com sucesso!");
            index = 0;
            loadedData = false;
        }
    }

    private string ConstructTextField(string labelName, string inputValue)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(labelName);
        inputValue = EditorGUILayout.TextField(inputValue, GUILayout.Width(75));
        EditorGUILayout.EndHorizontal();

        return inputValue;
    }
    private float ConstructFloatField(string labelName, float sliderValue, float maxValue)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(string.Format("{0}",labelName));
        sliderValue = EditorGUILayout.Slider(sliderValue, 0, maxValue);
        EditorGUILayout.EndHorizontal();

        return sliderValue;
    }
    private int ConstructIntField(string  labelName, int sliderValue, int maxValue)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(string.Format("{0}", labelName));
        sliderValue = (EditorGUILayout.IntSlider(sliderValue, 0, maxValue));
        EditorGUILayout.EndHorizontal();

        return sliderValue;
    }
    private System.Enum ConstructEnumField(string labelName, System.Enum value)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(labelName);
        value = (Chain)EditorGUILayout.EnumPopup(value);
        GUILayout.EndHorizontal();

        return value;
    }
    private int ConstructSelectionField(int index,string[] selections,  string label)
    {
        index = EditorGUILayout.Popup(label, index, selections);
        return index;
    }

    private void loadStation()
    {        
        systemPresentation.font = Font.CreateDynamicFontFromOSFont("Georgia", 12);
    }
    private void LoadYmirData(bool loadNameAsString)
    {
        MateriaSaveManager msm = new MateriaSaveManager(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\YMIRDATA.ODF", null);
        savedData = msm.GetYmir();
        List<string> names = new List<string>();

        if(loadNameAsString)
        {
            foreach(Ymir ymir in savedData) { names.Add(ymir.Nome); }
        }
        ymirNames = names.ToArray();
        Debug.Log("Dados carregados!");
    }
}
