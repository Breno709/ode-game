﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Convertson : MonoBehaviour {

    public Dropdown materia1, materia2, armazem1, armazem2;
    public Text saidaArm1, saidaArm2, resultado;
    public List<MateriaPreview> criacoes = new List<MateriaPreview>();
    public Toggle m1, m2, a1, a2;

    public void ProcessarDados()
    {
        MateriaPreview v1 = new ElementoBase();
        MateriaPreview v2 = new ElementoBase();
        if (m1.isOn){ v1 = GetMat(materia1.options[materia1.value].text); }
        else if (a1.isOn) { v1 = criacoes[armazem1.value - 3];}

        if (m2.isOn) { v2 = GetMat(materia2.options[materia2.value].text); }
        else if (a2.isOn) { v2 = criacoes[armazem2.value - 3]; }

        MateriaPreview result = MotorConversao.EBase.ConverterMateria(v1, v2);
        resultado.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", result.Nome, result.Almin, result.CargaMolar, result.NivelJoule);
        criacoes.Add(result);
        Dropdown.OptionData od = new Dropdown.OptionData(criacoes[criacoes.Count - 1].Nome);
        armazem1.options.Add(od);
        armazem2.options.Add(od);
    }
    
    public void RefreshData1()
    {
        MateriaPreview a1 = criacoes[armazem1.value - 3];
        saidaArm1.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", a1.Nome, a1.Almin, a1.CargaMolar, a1.NivelJoule);
    }

    public void RefreshData2()
    {
        MateriaPreview a2 = criacoes[armazem2.value - 3];
        saidaArm2.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", a2.Nome, a2.Almin, a2.CargaMolar, a2.NivelJoule);
    }

    public void ShowData1()
    {
        MateriaPreview v1 = GetMat(materia1.options[materia1.value].text);
        saidaArm1.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", v1.Nome, v1.Almin, v1.CargaMolar, v1.NivelJoule);
    }

    public void ShowData2()
    {
        MateriaPreview v1 = GetMat(materia2.options[materia2.value].text);
        saidaArm2.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", v1.Nome, v1.Almin, v1.CargaMolar, v1.NivelJoule);
    }


    MateriaPreview GetMat(string texto)
    {
        ElementoBase r;
        switch (texto)
        {
            case "Fogo":
                r = new ElementoBase("Fogo");
                r.Almin = Alma.AlIn;
                r.CargaMolar = InfoBase.Fogo_CM;
                r.NivelJoule = InfoBase.Fogo_NJ;
                return r;

            case "Agua":
                r = new ElementoBase("Agua");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Agua_CM;
                r.NivelJoule = InfoBase.Agua_NJ;
                return r;

            case "Vento":
                r = new ElementoBase("Vento");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Vento_CM;
                r.NivelJoule = InfoBase.Vento_NJ;
                return r;

            case "Terra":
                r = new ElementoBase("Terra");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Terra_CM;
                r.NivelJoule = InfoBase.Terra_NJ;
                return r;

            case "Eletricidade":
                r = new ElementoBase("Eletricidade");
                r.Almin = Alma.Elektron;
                r.CargaMolar = InfoBase.Eletricidade_CM;
                r.NivelJoule = InfoBase.Eletricidade_NJ;
                return r;

            case "Magnetismo":
                r = new ElementoBase("Magnetismo");
                r.Almin = Alma.Elektron;
                r.CargaMolar = InfoBase.Magnetismo_CM;
                r.NivelJoule = InfoBase.Magnetismo_NJ;
                return r;

            case "Luz":
                r = new ElementoBase("Luz");
                r.Almin = Alma.Espectronis;
                r.CargaMolar = InfoBase.Luz_CM;
                r.NivelJoule = InfoBase.Luz_NJ;
                return r;

            case "Som":
                r = new ElementoBase("Som");
                r.Almin = Alma.Espectronis;
                r.CargaMolar = InfoBase.Som_CM;
                r.NivelJoule = InfoBase.Som_NJ;
                return r;

            default:
                return new ElementoBase();
        }
    }
}
