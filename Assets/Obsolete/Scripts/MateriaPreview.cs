﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.IO;
using System.Reflection;

/// <summary>
/// Materia Ultrapassada
/// </summary>
public interface MateriaPreview {
    float NivelJoule { get; set; }
    Alma Almin { get; set; }
    string Nome { get; }
    /// <summary>
    /// Carga por mol
    /// </summary>
    /// <remarks>Carga é tambem a capacidade de causar dano comum (quando não há afinidade)</remarks>
    float CargaMolar { get; set; }
}

public enum Alma
{
    /// <summary>
    /// Particula elementar do fogo
    /// </summary>
    AlIn = 1,
    /// <summary>
    /// Particula elementar da agua, vento e terra
    /// </summary>
    Nature = 2,
    /// <summary>
    /// Particula elementar da eletricidade e magnetismo
    /// </summary>
    Elektron = 3,
    /// <summary>
    /// Particula elementar da luz e som
    /// </summary>
    Espectronis = 10,
    /// <summary>
    /// Particula de erro, capaz de roubar energia
    /// </summary>
    Nata = 0
}

/// <summary>
/// Informações padrão das bases
/// </summary>
public struct InfoBase
{
    static public float Fogo_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados=true; } return joules[0]; } }
    static public float Agua_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[1]; } }
    static public float Vento_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[2]; } }
    static public float Terra_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[3]; } }
    static public float Eletricidade_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[4]; } }
    static public float Magnetismo_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[5]; } }
    static public float Luz_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[6]; } }
    static public float Som_NJ { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules[7]; } }

    static public string Fogo_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[0]; } }
    static public string Agua_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[1]; } }
    static public string Vento_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[2]; } }
    static public string Terra_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[3]; } }
    static public string Eletricidade_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[4]; } }
    static public string Magnetismo_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[5]; } }
    static public string Luz_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[6]; } }
    static public string Som_A { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins[7]; } }

    static public float Fogo_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[0]; } }
    static public float Agua_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[1]; } }
    static public float Vento_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[2]; } }
    static public float Terra_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[3]; } }
    static public float Eletricidade_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[4]; } }
    static public float Magnetismo_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[5]; } }
    static public float Luz_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[6]; } }
    static public float Som_CM { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar[7]; } }

    static public float[] TodosJoules { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return joules.ToArray(); } }
    static public string[] TodasAlmas { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return almins.ToArray(); } }
    static public string[] TodosNomes { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return nomes.ToArray(); } }
    static public float[] TodasCargamolar { get { if (!dadosVerificados) { Check(); dadosVerificados = true; } return cargaMolar.ToArray(); ; } }

    static private List<string> nomes = new List<string>();
    static private List<string> almins = new List<string>();
    static private List<float> joules = new List<float>();
    static private List<float> cargaMolar = new List<float>();
    static private bool dadosVerificados = false;

    static private void Check()
    {
        System.IO.TextReader textReader = (TextReader)new StreamReader(@"C:\Users\Public\Documents\Unity Projects\Ode\Assets\Data\ElementoBaseDATA.xml");
        XmlTextReader reader = new XmlTextReader(textReader);

        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    switch (reader.Name)
                    {
                        case "Nome": nomes.Add(reader.ReadElementContentAsString()); break;
                        case "Almin": almins.Add(reader.ReadElementContentAsString()); break;
                        case "NivelJoule": joules.Add(reader.ReadElementContentAsFloat()); break;
                        case "CargaMolar": cargaMolar.Add(reader.ReadElementContentAsFloat()); break;
                    }
                    break;
            }
        }
        reader.Close();
        textReader.Close();
    }

    /// <summary>
    /// Calcula o nível joulico maximo de um elemento base a partir de seu padrao
    /// </summary>
    /// <param name="joulePadrãoElemento">Nível joulico padrao do elemento a ser calculado</param>
    static public float Max(float joulePadrãoElemento)
    { return joulePadrãoElemento + (joulePadrãoElemento / 10);}

    /// <summary>
    /// Calcula o nível joulico minimo de um elemento base a partir de seu padrao
    /// </summary>
    /// <param name="joulePadrãoElemento">Nível joulico padrao do elemento a ser calculado</param>
    static public float Min(float joulePadrãoElemento)
    { return joulePadrãoElemento - (joulePadrãoElemento / 10); }

    /// <summary>
    /// Tenta conveter o texto em Almin
    /// </summary>
    /// <param name="entrada">texto a ser convertido</param>
    static public Alma ToAlmin(string entrada)
    {
        switch(entrada.ToUpper())
        {
            case "ALIN": return Alma.AlIn;
            case "NATURE": return Alma.Nature;
            case "ELEKTRON": return Alma.Elektron;
            case "ESPECTRONIS": return Alma.Espectronis;
            case "NATA": return Alma.Nata;
            default: throw new Exception("Não foi possível converter a entrada");
        }
    }
}

public class ElementoBase : MateriaPreview
{
    public string Nome { get { return nome; } }
    public Alma Almin { get; set; }
    public float NivelJoule { get; set; }
    public float CargaMolar { get; set; }
    public string Funcao { get; set; }

    public ElementoBase()
    {
        nome = "Não Classificado";
    }

    public ElementoBase(string nomeElemento)
    {
        nome = nomeElemento;
    }

    private string nome;
    public MateriaPreview Reacao(params MateriaPreview[] materiais)
    {
        Type tipo = Type.GetType(this.Nome);
        ConstructorInfo construtor = tipo.GetConstructor(Type.EmptyTypes);
        object designado = construtor.Invoke(new object[] { }); //verificar esta condição

        MethodInfo metodo = tipo.GetMethod(this.Funcao);
        object valor = metodo.Invoke(designado, new object[] { materiais });

        return (MateriaPreview)valor;
    }
}