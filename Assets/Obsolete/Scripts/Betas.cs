﻿using UnityEngine;
using System.Collections;
using System;

//Caso haja futuramente problemas de acessibilidade, acrescentar public antes da interface
interface Betas : MateriaPreview {
    /// <summary>
    /// Almin geratrizes
    /// </summary>
    Alma[] Criacao { get; }
    /// <summary>
    /// Materia de mesmo Almin geratriz mas incapaz de gerá-lo
    /// </summary>
    MateriaPreview[] Restricao { get; }
    /// <summary>
    /// Almin cuja eficiencia para destruí-la é maior
    /// </summary>
    Alma AfinidadeDestrutiva { get; }
    /// <summary>
    /// Almin cuja eficiencia de se proteger contra ataques da mesma é maior
    /// </summary>
    Alma AfinidadeDefensiva { get; }
}

[Serializable]
public class BetaBase : Betas
{
    public string Nome { get { return nome; } }
    public Alma AfinidadeDefensiva { get { return defesa; } }
    public Alma AfinidadeDestrutiva { get { return ofensa; } }

    public float CargaMolar{ get; set; }
    public Alma[] Criacao { get { return criacao; } }
    public MateriaPreview[] Restricao { get { return restricao; } }

    public Alma Almin { get { return almin; } set { } }
    public float NivelJoule { get; set; }

    /// <summary>
    /// Nome da classe que define as funções
    /// </summary>
    public string FuncaoClasse { get; set; }
    /// <summary>
    /// Nome das Funções descritas
    /// </summary>
    public string[] Funcao { get; set; }

    private Alma[] criacao = { Alma.AlIn, Alma.Nature };
    private MateriaPreview[] restricao;
    private string nome;
    private Alma defesa;
    private Alma ofensa;
    private Alma almin;

    public BetaBase()
    { nome = "Não Classificado"; }

    public BetaBase (string nomeBeta, Alma defensividade, Alma destrutividade, float cargaMolar, Alma[] parentesco, ElementoBase[] restringimento, Alma origem, float joule, string classeFuncao, string[] funcoes)
    {
        nome = nomeBeta;
        defesa = defensividade;
        ofensa = destrutividade;
        CargaMolar = cargaMolar;
        criacao = parentesco;
        restricao = restringimento;
        almin = origem;
        NivelJoule = joule;
        FuncaoClasse = classeFuncao;
        Funcao = funcoes;
    }

    public BetaBase(BetaBase materia)
    {
        nome = materia.Nome;
        defesa = materia.AfinidadeDefensiva;
        ofensa = materia.AfinidadeDestrutiva;
        CargaMolar = materia.CargaMolar;
        criacao = materia.Criacao;
        restricao = materia.Restricao;
        almin = materia.Almin;
        NivelJoule = materia.NivelJoule;
    }
}