﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Xamal : MonoBehaviour {
    public Dropdown materia1, materia2;
    public Text saida;

    public void ProcessarDados()
    {
        ElementoBase elementoFuncional = new ElementoBase("Fogo");
        elementoFuncional.Funcao = "ConstruirBase";
        MateriaPreview result = elementoFuncional.Reacao(GetMat(materia1.options[materia1.value].text), GetMat(materia1.options[materia2.value].text));

        saida.text = string.Format("Nome: {0}\nAlma: {1}\nCarga Molar: {2}\nNível Joule: {3}", result.Nome, result.Almin, result.CargaMolar, result.NivelJoule);
    }

    MateriaPreview GetMat(string texto)
    {
        ElementoBase r;
        switch (texto)
        {
            case "Fogo":
                r = new ElementoBase("Fogo");
                r.Almin = Alma.AlIn;
                r.CargaMolar = InfoBase.Fogo_CM;
                r.NivelJoule = InfoBase.Fogo_NJ;
                return r;

            case "Agua":
                r = new ElementoBase("Agua");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Agua_CM;
                r.NivelJoule = InfoBase.Agua_NJ;
                return r;

            case "Vento":
                r = new ElementoBase("Vento");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Vento_CM;
                r.NivelJoule = InfoBase.Vento_NJ;
                return r;

            case "Terra":
                r = new ElementoBase("Terra");
                r.Almin = Alma.Nature;
                r.CargaMolar = InfoBase.Terra_CM;
                r.NivelJoule = InfoBase.Terra_NJ;
                return r;

            case "Eletricidade":
                r = new ElementoBase("Eletricidade");
                r.Almin = Alma.Elektron;
                r.CargaMolar = InfoBase.Eletricidade_CM;
                r.NivelJoule = InfoBase.Eletricidade_NJ;
                return r;

            case "Magnetismo":
                r = new ElementoBase("Magnetismo");
                r.Almin = Alma.Elektron;
                r.CargaMolar = InfoBase.Magnetismo_CM;
                r.NivelJoule = InfoBase.Magnetismo_NJ;
                return r;

            case "Luz":
                r = new ElementoBase("Luz");
                r.Almin = Alma.Espectronis;
                r.CargaMolar = InfoBase.Luz_CM;
                r.NivelJoule = InfoBase.Luz_NJ;
                return r;

            case "Som":
                r = new ElementoBase("Som");
                r.Almin = Alma.Espectronis;
                r.CargaMolar = InfoBase.Som_CM;
                r.NivelJoule = InfoBase.Som_NJ;
                return r;

            default:
                return new ElementoBase();
        }
    }
}
