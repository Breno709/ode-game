﻿using System;
using UnityEngine;
using System.Collections;

public interface IProduto
{
    string Nome { get; set; }
    string Descricao { get; set; }
    int Preço { get; set; }
    string Manufatureira { get; set; }
    ComponenteVersao VersaoProduto { get; set; }
}
