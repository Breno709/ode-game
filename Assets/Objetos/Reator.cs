﻿using UnityEngine;
using System.Collections;

public class Reator : Componente, IProduto {
    public string Nome { get; set; }
    public string Descricao { get; set; }
    public string Manufatureira { get; set; }
    public ComponenteVersao VersaoProduto {get;set;}
    public int Preço { get; set; }

    public virtual ElementoBase ReagirBase (MateriaPreview reagente, params MateriaPreview[] entradas)
    {
        return null;
    }
}