﻿using System;
using UnityEngine;
using System.Collections;

public interface Componente
{
    string Nome { get; set; }
    string Manufatureira { get; set; }
    ComponenteVersao VersaoProduto { get; set; }
}

public struct ComponenteVersao
{
    int Major { get; set; }
    int Minor { get; set; }
    DateTime Data { get; set; }

    public ComponenteVersao(int major, int minor, DateTime dataCriacao)
    { Major = major; Minor = minor; Data = dataCriacao; }

    public override string ToString()
    {
        return string.Format("{0}.{1} - {2}", Major, Minor, Data.ToShortDateString());
    }
}
